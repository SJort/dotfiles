if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin()
Plug 'lervag/vimtex'
call plug#end()
let g:tex_flavor = 'latex'
let g:vimtex_view_method = 'zathura'
set packpath+=~/.vim/pack/



"auto indent in if statements etc when using 'o' for example
:set smartindent

" enable backspace in insert mode
set backspace=2
set backspace=indent,eol,start

" timestamp map
nmap <F2> i<C-R>=strftime("//jort @ %Y-%m-%d: ")<CR><Esc>
imap <F2> <C-R>=strftime("//jort @ %Y-%m-%d: ")<CR>

"fill line function
map <F3> 180A/<Esc>d180|

" line numbers
set number

" status bar
set laststatus=2

" use x clipboard
set clipboard=unnamedplus

" case insensitive search except when cases are used in search
:set ignorecase
:set smartcase

set relativenumber

" cursor scrolls
set mouse=a

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'lervag/vimtex'
call plug#end()

let g:tex_flavor = 'latex'
let g:vimtex_view_method = 'zathura'

set packpath+=~/.vim/pack/

set t_Co=256   " This is may or may not needed.

set background=light
colorscheme PaperColor

" line numbers
set number
" status bars
set laststatus=2
" x clipboard
set clipboard=unnamedplus
"
set relativenumber
" cursor scrolls
set mouse=a
" keep folds
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview

" write file with root permissions: w!!
cmap w!! w !sudo tee > /dev/null %et ignorecase

" disable sound error

set visualbell
set noerrorbells

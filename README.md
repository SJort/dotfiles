# Dotfiles
Operating system: [Arch Linux](https://wiki.archlinux.org/)  
Window manager: [i3-wm](https://i3wm.org/docs/userguide.html)  
Editor: [gvim](https://vim.fandom.com/wiki/Vim_Tips_Wiki)  
Dotfiles manager: [yadm-git](https://yadm.io/)  

# Using
Yadm is kind of a wrapper for git, and it works with most git commands.  

Upload changes  
```shell
yadm add -u
yadm commit -m "message"
yadm push
```
